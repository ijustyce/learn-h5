function register() {
  var result = $("#myModal");
  var nameInput = $("#user").val();
  if (nameInput.length < 5) {
    result.find("#myModalContent").text("用户名不能少于5个字符！");
    result.modal({keyboard: true});
    return;
  }
  var mail = $("#mail").val();
  var filter  = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (!filter.test(mail)) {
    result.find("#myModalContent").text("邮箱格式错误!");
    result.modal({keyboard: true});
    return;
  }
  var pw = $("#password").val();
  if (pw.length < 6) {
    result.find("#myModalContent").text("密码不能少于6个字符！");
    result.modal({keyboard: true});
    return;
  }

  var user = {
    username:nameInput,
    password:pw,
    email:mail
  };

  $.ajax({
			url: 'http://localhost:8080/webapp/addUser.do',
			type: 'post',
      dataType: 'text',
      data:user,
			cache : false,
			success: function(data) {
        result.find("#myModalContent").text("感谢你注册，系统已发送激活邮件，请您激活账号！");
        result.find("#myModalTitle").text("注册成功");
        result.modal({keyboard: true});
			},
      error : function(result){
        result.find("#myModalContent").text("注册失败！失败函数被回调！");
        result.find("#myModalTitle").text("注册失败");
        result.modal({keyboard: true});
      }
		});
}
